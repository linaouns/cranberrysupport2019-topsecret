Documentez votre stratégie de tests, vos tests, et les bugs trouvés dans ce fichier.
- Classe "Client"
    La/Les méthode(s) testée(s) sont/est:
        ajoutRequete() -> test si l'ajout d'une requêtes est fait

- Classe "Technicien"
    La/Les méthode(s) testée(s) sont/est:
        ajouterRequeteAssignee() -> test si l'ajout d'une requête dans la liste des requêtes en cours et dans la liste des requêtes du technicien est fait
        ajoutListRequetesFinies() -> test si l'ajout d'une requête dans la liste des requêtes en cours et dans la liste des requêtes finalisées
        ajoutRequete -> test si l'ajout d'une requête est bien faite dans les liste des requêtes du technicien (ceux qui lui sont assignées et qu'il a fait)

- Classe "Requete"
    La/Les méthode(s) testée(s) sont/est:
        addCommentaire() -> test si l'ajout d'une commentaire à une requête a un utilisateur comme param
        setSujet() -> test du setter de sujet
        setDescription() -> test du setter de sujet
        setCategorie() -> test du setter de sujet

- Classe "BanqueRequetes"
        *Un constructeur qui prend un paramètre un scanner est créer pour pouvoir mocker quelque de ses méthodes
    La/Les méthode(s) testée(s) est/sont:
        chargerRequetes() -> test si les requêtes sont chargées
        saveRequetes() -> test si les requêtes sont sauvées
        returnLast() -> test si la requête retourne le dernier élément
        newRequete() -> test l'ajout d'une nouvelle requête

- Classe "BanquesUtilisateurs"
        *Un constructeur qui prend un paramètre un scanner est créer pour pouvoir mocker quelque de ses méthodes
    La/Les méthode(s) testée(s) sont/est:
       addUtilisateur() -> test si l'ajout d'un utilisateur en utilisant une String marche correctement
       chercherUtilisateur() -> test la recherche d'utilisateur (avec utilisateur comme param)
       chercherNomRole() -> test la recherche d'utilisateur (avec nom d'un utilisateur comme param)
       chargerUtilisateur() -> test le chargement/loading du fichier BanqueUtilisateur.txt
       assignerRequete() -> test l'assignement d'un technicien à une requête