Documentez l'ajout de fonctionnalités dans ce fichier.

Fonctionnalité #1
	Un type d'utilisateur est ajouté (Administrateur), l'ajout d'une variable d'instance dans Utilisateur pour déterminer l'état d'un compte (fermé) et d'une méthode (closeAccount) qui bloque le login au utilisateur. Pour les techniciens, la méthode désassigne les requetes qui sont attribuée a l'utilisateur en question.

Fonctionnalité #2
	Deux boutons sont ajoutées à l'interface graphique qui permettent aux clients de faire savoir au techniciens s'ils sont satisfait de la résolution d'une requête