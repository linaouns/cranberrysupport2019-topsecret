Documentez votre stratégie de remaniement et vos modifications dans ce fichier.

Modifications générale:
	Intendation du code
	Commentaires supprimés
	
	Import inutilisés enlevés
	Nom des variables/paramètres/méthodes plus significatifs
	"Meilleur" Encapsulation 
	Large Class (Requetes)


Modification spécifique aux classes:

---VIEW---

Parler du MVC

---MODEL---

Classe "Utilisateur"
	- Méthode fetchInfos() et variable d'instance info supprimées (inutilisées)
	- Encapsulation de la variable d'instance role -> public à protected
	- Ajout de getter/setter pour les variables d'instance
	- getNom() et getNomUtil() retourne la même variable que getNomUtil(), getNom() retourne maintenant le nom de l'utilisateir

Classe "Client"
	- Suppression de cette classe

Classe "Technicien"
	- Hérite maintenant du client (mnt Utilisateur) car un technicien peut faires toutes les opérations d'un client
	
Classe "Utilisateur"
	- N'est plus abstract, représente le client


Classe "Requete"
    - Classe trop large -> Les enums Catégorie et Statut sont extractés en dehors de la classe dans des fichiers séparés. (Catégorie.java et Statut.java)
	- Initialisation redondante de variable d'instance enlevée (sujet, description)
	- Suppresion de la variable d'instance pathFichier (inutilisé)
	- Suppression de la méthode finaliser jamais utilisé ***************** a implémenter?
	- Suppression de la méthode uploadFichier() (la dépendance aux fichiers doit être enlevée)
	- Ajout d'une variable d'instance (satisfaction) pour la fonctionalité #2
	
Classe "Commentaire"
	- Encapsulation protected à private pour les variables d'instances
	
Classe "BanqueRequetes"
	- ArrayList des requêtes avec une capacité par défaut de 100, aucune raison de choisir la capacité puisque les arraylist se redimensionne automatiquement
	- Rajout d'un DAO pour charger et sauver les requêtes 
	
Classe "BanqueUtilisateur"
	- ArrayList des requêtes avec une capacité par défaut de 100, aucune raison de choisir la capacité puisque les arraylist se redimensionne automatiquement
	- Rajout d'un DAO pour charger et sauver les utilisateurs
	

---DAO---

Une DAO a été introduit pour pouvoir isoler la logique d'affaire de la persistence, il permet d'introduire des nouvelles méthodes de stockage (h2) grace  une couche d'abstraction. Il facilite aussi les tests car ils nous permet de mocker les classes interagissant avec la base de données et les fichiers lorsqu'on teste nos services. L'introduction de différente base de données ou méthode de stockage (.txt, .xls etc.) sans affecter notre code et ni les tests.
	
Interface "RequetesDAO"
	-Couche d'abstraction DAO pour le stockage des requêtes
Classe RequetesDAOH2Impl
	Implémentation H2 de tout ce qui porte à la persistence de données (requete)
Classe RequetesDAOTextFileImpl
	Implémentation par defaut (fichiers .txt) de tout ce qui porte à la persistence de données (requete)


Interface "UtilisateursDAO"
	-Couche d'abstraction DAO pour le stockage d'utilisateur
Classe UtilisateurDAOH2Impl
	Implémentation H2 de tout ce qui porte à la persistence de données (utilisateur)
Classe UtilisateurDAOTextFileImpl
	Implémentation par defaut (fichiers .txt) de tout ce qui porte à la persistence de données (utilisateurs)
	
	


