package controllers;

import models.Utilisateur;
import services.BanqueUtilisateurs;
import views.*;

import java.io.IOException;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NotLoggedController {

    private static String ERROR_FETCHING_REPOSITORY = "La connexion avec la base de données n'a pas pu être établie. " +
            "Veuillez contacter un de nos techniciens!";
    private static String ERROR_LOG_AS_TECHNICIEN = "Vous n'êtes pas un technicien! Tentez de vous reconnecter en temps que client.";
    private DemarrageFrame mainFrame;
    private LoginFrame loginFrame;

    public NotLoggedController() {
        mainFrame = new DemarrageFrame();
        initVue();
    }

    public void initVue() {
        mainFrame.getLogAsTechnicienBtn().addActionListener(e -> {
            loginFrame = new TechLoginFrame(mainFrame);
            loginFrame.getLoginBtn().addActionListener(evt ->
                    onLoginBtnClick(utilisateur -> onLogAsTechnicienBtnClick(utilisateur))
            );
        });

        mainFrame.getLogAsClientBtn().addActionListener(event -> {
            loginFrame = new ClientLoginFrame(mainFrame);
            loginFrame.getLoginBtn().addActionListener(evt ->
                    onLoginBtnClick(utilisateur -> new ClientLoggedController(utilisateur))
            );
        });
    }

    private void onLoginBtnClick(Consumer<Utilisateur> logUtilisateurSelonRole) {
        String nomUtilisateur = loginFrame.getNomUtilisateurInput().getText();
        String motDePasse = loginFrame.getMotDePasseInput().getText();

        Utilisateur utilisateurTrouve = BanqueUtilisateurs.getUserInstance().chercheUtilisateurParUserName(nomUtilisateur);
        if (utilisateurTrouve != null && utilisateurTrouve.login(nomUtilisateur, motDePasse) && !utilisateurTrouve.isLocked()) {
            logUtilisateurSelonRole.accept(utilisateurTrouve);
        } else {
            new ErrorLoginFrame(loginFrame);
        }
        mainFrame.setVisible(false);
        loginFrame.setVisible(false);
    }

    private void onLogAsTechnicienBtnClick(Utilisateur utilisateur) {
        if (utilisateur.getRole().equals(Utilisateur.Role.technicien.toString())) {
            try {
                new TechLoggedController(utilisateur);
            } catch (IOException e) {
                Logger.getLogger(NotLoggedController.class.getName()).log(Level.SEVERE, null, e);
                new ErrorLoginFrame(loginFrame, ERROR_FETCHING_REPOSITORY);
            }
        } else {
            new ErrorLoginFrame(loginFrame, ERROR_LOG_AS_TECHNICIEN);
        }
    }

}
