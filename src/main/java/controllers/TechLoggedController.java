package controllers;

import models.Statut;
import models.Utilisateur;
import services.BanqueRequetes;
import services.BanqueUtilisateurs;
import views.TechLoggedFrame;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TechLoggedController implements LoggedController {

    private Utilisateur loggedUtilisateur;
    private TechLoggedFrame techLoggedFrame;

    public TechLoggedController(Utilisateur loggedUtilisateur) throws IOException {
        this.loggedUtilisateur = loggedUtilisateur;
        techLoggedFrame = new TechLoggedFrame(loggedUtilisateur);
        initView();
        techLoggedFrame.setVisible(true);
    }

    private void initView() throws IOException {
        techLoggedFrame.setRequetesDispos(BanqueRequetes.getInstance().getListRequetesByStatut(Statut.ouvert));
        techLoggedFrame.setTechniciens(BanqueUtilisateurs.getUserInstance()
                .getUtilisateursByRole(Utilisateur.Role.technicien.toString()));
        techLoggedFrame.getAddRequeteBtn().addActionListener(e -> onAddRequeteBtnClick());
    }

    private void onAddRequeteBtnClick() {
        new NewRequeteController(loggedUtilisateur, this, techLoggedFrame);
    }

    @Override
    public void reset() {
        try {
            new TechLoggedController(loggedUtilisateur);
        } catch (IOException e) {
            Logger.getLogger(TechLoggedController.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
