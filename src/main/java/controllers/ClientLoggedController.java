package controllers;

import models.Utilisateur;
import services.BanqueRequetes;
import views.ClientLoggedFrame;

public class ClientLoggedController implements LoggedController {

    private Utilisateur loggedUtilisateur;
    private ClientLoggedFrame clientLoggedFrame;

    public ClientLoggedController(Utilisateur loggedUtilisateur) {
        this.loggedUtilisateur = loggedUtilisateur;
        clientLoggedFrame = new ClientLoggedFrame(loggedUtilisateur);
        initView();
    }

    private void initView() {
        clientLoggedFrame.getAddRequeteBtn().addActionListener(e -> onAddRequeteBtnClick());
        clientLoggedFrame.getQuitBtn().addActionListener(evt -> onQuitBtnClick());
    }

    private void onAddRequeteBtnClick() {
        new NewRequeteController(loggedUtilisateur, this, clientLoggedFrame);
    }

    private void onQuitBtnClick() {
        BanqueRequetes.getInstance().saveRequetes();
        System.exit(0);
    }

    @Override
    public void reset() {
        new ClientLoggedController(loggedUtilisateur);
    }

}
