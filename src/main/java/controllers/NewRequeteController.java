package controllers;

import models.Categorie;
import models.Utilisateur;
import services.BanqueRequetes;
import views.NewRequeteFrame;

import javax.swing.*;

public class NewRequeteController {

    private Utilisateur loggedUtilisateur;
    private LoggedController controllerPrecedent;
    private NewRequeteFrame newRequeteFrame;

    public NewRequeteController(Utilisateur loggedUtilisateur, LoggedController controllerPrecedent, JFrame framePrecedent) {
        this.loggedUtilisateur = loggedUtilisateur;
        this.controllerPrecedent = controllerPrecedent;
        newRequeteFrame = new NewRequeteFrame(framePrecedent);
        newRequeteFrame.getCreerRequeteBtn().addActionListener(evt -> onCreerRequeteBtnClick());
    }

    private void onCreerRequeteBtnClick() {
        BanqueRequetes.getInstance().newRequete(
                newRequeteFrame.getSujetFld().getText(),
                newRequeteFrame.getDescpArea().getText().replaceAll("\n", " "),
                loggedUtilisateur,
                Categorie.fromString((String) newRequeteFrame.getCatBox().getSelectedItem())
        );

        BanqueRequetes.getInstance().returnLast().setFile(newRequeteFrame.getFile());

        newRequeteFrame.setVisible(false);
        controllerPrecedent.reset();
    }

}
