package services;

import dao.UtilisateursDAO;
import dao.UtilisateursDAOH2Impl;
import models.Administrateur;
import models.Technicien;
import models.Utilisateur;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class BanqueUtilisateurs {

    private static BanqueUtilisateurs instance = null;
    private static UtilisateursDAO utilisateursDAO;
    private ArrayList<Utilisateur> utilisateurs;

    private BanqueUtilisateurs() {
        utilisateurs = new ArrayList<>();
    }

    public static BanqueUtilisateurs getUserInstance() {
        if (instance != null) {
            return instance;
        } else {
            instance = new BanqueUtilisateurs();
            if (utilisateursDAO == null) {
                try {
                    utilisateursDAO = UtilisateursDAOH2Impl.getInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            instance.chargerUtilisateur();
            return instance;
        }
    }

    public static BanqueUtilisateurs getUserInstance(UtilisateursDAO pUtilisateursDAO) throws FileNotFoundException {
        utilisateursDAO = pUtilisateursDAO;
        instance = null;
        return getUserInstance();
    }

    public ArrayList<Utilisateur> getUtilisateurs() {
        return utilisateurs;
    }

    public void chargerUtilisateur() {
        utilisateurs = new ArrayList<>(utilisateursDAO.getAll());
    }

    public ArrayList getUtilisateursByRole(String role) {
        ArrayList<Utilisateur> utilisateursByRole = new ArrayList<>();
        for (Utilisateur utilisateur : utilisateurs) {
            if (utilisateur.getRole().equals(role))
                utilisateursByRole.add(utilisateur);
        }
        return utilisateursByRole;
    }

    public Utilisateur chercheUtilisateurParUserName(String utilisateurName) {
        if (utilisateurName != null) {
            for (Utilisateur utilisateur : utilisateurs) {
                if (utilisateur.getNomUtilisateur().equalsIgnoreCase(utilisateurName))
                    return utilisateur;
            }
        }
        return null;
    }

    public void lockAccount(String username) {
        Utilisateur utilisateur = chercheUtilisateurParUserName(username);
        utilisateur.closeAccount();
    }

    public void registerAccount(String prenom, String nom, String nomUtilisateur, String mdp, String role) {
        utilisateurs.add(
                role.equals(Utilisateur.Role.technicien.toString())
                        ? new Technicien(prenom, nom, nomUtilisateur, mdp)
                        : (
                        role.equals(Utilisateur.Role.administrateur.toString())
                                ? new Administrateur(prenom, nom, nomUtilisateur, mdp)
                                : new Utilisateur(prenom, nom, nomUtilisateur, mdp)
                )
        );
    }

    public int saveRequetes() {
        return utilisateursDAO.save(utilisateurs);
    }
}