package services;

import dao.RequetesDAO;
import dao.RequetesDAOH2Impl;
import models.Categorie;
import models.Requete;
import models.Statut;
import models.Utilisateur;

import java.util.ArrayList;

public class BanqueRequetes {

    private static BanqueRequetes instance = null;
    private static RequetesDAO requetesDAO;
    private Integer numero = -1;
    private ArrayList<Requete> listeRequetes;

    private BanqueRequetes() {
        listeRequetes = new ArrayList<Requete>();
    }

    public static BanqueRequetes getInstance() {
        if (instance != null) {
            return instance;
        } else {
            instance = new BanqueRequetes();
            if (requetesDAO == null) {
                try {
                    requetesDAO = RequetesDAOH2Impl.getInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            instance.chargerRequetes();
            return instance;
        }
    }

    public static BanqueRequetes getInstance(RequetesDAO pRequetesDAO) {
        requetesDAO = pRequetesDAO;
        instance = null;
        return getInstance();
    }

    public void newRequete(String sujet, String description, Utilisateur client, Categorie categorie) {
        Requete nouvelle = new Requete(sujet, description, client, categorie);
        listeRequetes.add(nouvelle);
        client.ajouterRequeteCreee(nouvelle);
    }

    public ArrayList<Requete> getListeRequetes() {
        return listeRequetes;
    }

    public ArrayList getListRequetesByStatut(Statut statut) {
        ArrayList<Requete> requetes = new ArrayList<Requete>();
        for (Requete requete : listeRequetes) {
            if (requete.getStatut().equals(statut))
                requetes.add(requete);
        }
        return requetes;
    }

    public Requete returnLast() {
        return listeRequetes.get(listeRequetes.size() - 1);
    }

    private void chargerRequetes() {
        listeRequetes = new ArrayList<>(requetesDAO.getAll());
    }

    public int saveRequetes() {
        return requetesDAO.save(listeRequetes);
    }
}
