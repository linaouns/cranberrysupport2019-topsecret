package views;

import javax.swing.*;

public class DemarrageFrame extends javax.swing.JFrame {

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel demarrageLbl;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JButton logAsClientBtn;
    private javax.swing.JButton logAsTechnicienBtn;

    public DemarrageFrame() {
        initComponents();
        setVisible(true);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFrame1 = new javax.swing.JFrame();
        demarrageLbl = new javax.swing.JLabel();
        logAsTechnicienBtn = new javax.swing.JButton();
        logAsClientBtn = new javax.swing.JButton();

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
                jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
                jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));

        setName("Démarrage");
        demarrageLbl.setText("Démarrer l'application en tant que:");
        logAsTechnicienBtn.setText("Technicien");
        logAsClientBtn.setText("Utilisateur");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        initLayoutHorizontalGroup(layout);
        initLayoutVerticalGroup(layout);
        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void initLayoutHorizontalGroup(javax.swing.GroupLayout layout) {
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addComponent(demarrageLbl)
                                .addGap(35, 35, 35)
                                .addComponent(logAsClientBtn)
                                .addGap(18, 18, 18)
                                .addComponent(logAsTechnicienBtn)
                                .addContainerGap(42, Short.MAX_VALUE))
        );
    }

    private void initLayoutVerticalGroup(javax.swing.GroupLayout layout) {
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(demarrageLbl)
                                        .addComponent(logAsClientBtn)
                                        .addComponent(logAsTechnicienBtn))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }

    public JButton getLogAsClientBtn() {
        return logAsClientBtn;
    }

    public JButton getLogAsTechnicienBtn() {
        return logAsTechnicienBtn;
    }
    // End of variables declaration//GEN-END:variables
}
