package views;

import models.*;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TechLoggedFrame extends javax.swing.JFrame {

    DefaultListModel requetesAssigneesListModel = new DefaultListModel();
    DefaultListModel requetesDisposListModel = new DefaultListModel();
    DefaultListModel techniciensListModel = new DefaultListModel();
    private Technicien loggedTechnicien;
    private List<Requete> requetesAssignees;
    private List<Requete> requetesDispos;
    private List<Technicien> techniciens;
    private Requete requeteSelectionnee;
    private File file;
    private boolean showComments = false;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel addComLbl;
    private javax.swing.JButton affRapportBtn;
    private javax.swing.JButton ajoutfichierBtn;
    private javax.swing.JButton assignerReqBtn;
    private javax.swing.JComboBox catBox;
    private javax.swing.JTextField comIn;
    private javax.swing.JLabel comLbl;
    private javax.swing.JTextArea commentsArea;
    private javax.swing.JTextArea descReqArea;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel fileLbl;
    private javax.swing.JComboBox finBox;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JButton addRequeteBtn;
    private javax.swing.JButton prendrereqBtn;
    private javax.swing.JButton quitBtn;
    private javax.swing.JLabel reqAssignedLbl;
    private javax.swing.JList reqAssignee;
    private javax.swing.JList reqDispo;
    private javax.swing.JLabel reqDispoLbl;
    private javax.swing.JLabel reqSelLbl;
    private javax.swing.JTextArea requeteArea;
    private javax.swing.JList supportList;
    public TechLoggedFrame(Utilisateur utilisateur) {
        initComponents();
        setVisible(false);
        loggedTechnicien = (Technicien) utilisateur;
        setRequetesAssignees(((Technicien) utilisateur).getRequetesAssignees());
    }

    private void setRequetesAssignees(List<Requete> requetes) {
        requetesAssignees = requetes;
        if (requetesAssignees.isEmpty()) {
            requetesAssigneesListModel.addElement("Vous n'avez pas de requête encore.");
            reqAssignee.setModel(requetesAssigneesListModel);
        } else {
            requetesAssignees.forEach(requete -> requetesAssigneesListModel.addElement(requete.getSujet()));
            reqAssignee.setModel(requetesAssigneesListModel);
        }
    }

    public void setRequetesDispos(ArrayList<Requete> requetes) {
        requetesDispos = requetes;
        if (requetesDispos.isEmpty()) {
            requetesDisposListModel.addElement("Il n'y a pas de requêtes pour le moment.");
            reqDispo.setModel(requetesDisposListModel);
            prendrereqBtn.setEnabled(false);
        } else {
            requetesDispos.forEach(requete -> requetesDisposListModel.addElement(requete.getSujet()));
            reqDispo.setModel(requetesDisposListModel);
            prendrereqBtn.setEnabled(true);
        }
    }

    public void setTechniciens(ArrayList<Technicien> techniciens) {
        this.techniciens = techniciens;
        if (techniciens.isEmpty()) {
            techniciensListModel.addElement("Il n'y a pas de technicien");
            supportList.setModel(techniciensListModel);
        } else {
            techniciens.forEach(tech -> techniciensListModel.addElement(tech.getNomUtilisateur()));
            supportList.setModel(techniciensListModel);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        fileChooser = new javax.swing.JFileChooser();
        reqAssignedLbl = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        reqAssignee = new javax.swing.JList();
        reqSelLbl = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        requeteArea = new javax.swing.JTextArea();
        comLbl = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        commentsArea = new javax.swing.JTextArea();
        addComLbl = new javax.swing.JLabel();
        comIn = new javax.swing.JTextField();
        catBox = new javax.swing.JComboBox();
        reqDispoLbl = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        reqDispo = new javax.swing.JList();
        prendrereqBtn = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        descReqArea = new javax.swing.JTextArea();
        addRequeteBtn = new javax.swing.JButton();
        assignerReqBtn = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        supportList = new javax.swing.JList();
        finBox = new javax.swing.JComboBox();
        ajoutfichierBtn = new javax.swing.JButton();
        affRapportBtn = new javax.swing.JButton();
        quitBtn = new javax.swing.JButton();
        fileLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        reqAssignedLbl.setText("Les requêtes qui vous sont assignées:");

        reqAssignee.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                reqAssigneeValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(reqAssignee);

        reqSelLbl.setText("Requête sélectionnée:");

        requeteArea.setColumns(20);
        requeteArea.setRows(5);
        jScrollPane2.setViewportView(requeteArea);

        comLbl.setText("Commentaires:");

        commentsArea.setColumns(20);
        commentsArea.setRows(5);
        jScrollPane3.setViewportView(commentsArea);

        addComLbl.setText("Ajouter commentaire:");

        comIn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comInKeyPressed(evt);
            }
        });

        catBox.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"Changer la catégorie", "Poste de travail", "Serveur", "Service web", "Compte usager", "Autre"}));
        catBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                catBoxItemStateChanged(evt);
            }
        });

        reqDispoLbl.setText("Requêtes disponibles non-assignée:");

        reqDispo.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                reqDispoValueChanged(evt);
            }
        });
        jScrollPane4.setViewportView(reqDispo);

        prendrereqBtn.setText("Prendre la requête sélectionnée");
        prendrereqBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prendrereqBtnActionPerformed(evt);
            }
        });

        descReqArea.setColumns(20);
        descReqArea.setRows(5);
        jScrollPane5.setViewportView(descReqArea);

        addRequeteBtn.setText("Créer une nouvelle requête");

        assignerReqBtn.setText("Assigner la requête à ce membre");
        assignerReqBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignerReqBtnActionPerformed(evt);
            }
        });

        jScrollPane6.setViewportView(supportList);

        finBox.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"Finaliser", "Succès", "Abandon", " "}));
        finBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                finBoxItemStateChanged(evt);
            }
        });

        ajoutfichierBtn.setText("Ajouter un fichier");
        ajoutfichierBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ajoutfichierBtnActionPerformed(evt);
            }
        });

        affRapportBtn.setText("Afficher le rapport");
        affRapportBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                affRapportBtnActionPerformed(evt);
            }
        });

        quitBtn.setText("Quitter");
        quitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitBtnActionPerformed(evt);
            }
        });

        fileLbl.setText("(pas de fichier attaché)");
        fileLbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fileLblMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        initLayoutHorizontalGroup(layout);
        initLayoutVerticalGroup(layout);
        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Quitter
    private void quitBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitBtnActionPerformed
        System.exit(0);
    }//GEN-LAST:event_quitBtnActionPerformed

    //On change la sortie du TextArea en fonction de la requete selectionnée
    private void reqAssigneeValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_reqAssigneeValueChanged
        updateRequeteA();
        updateCommentaires();
    }//GEN-LAST:event_reqAssigneeValueChanged

    //On change la sortie du TextArea en fonction de la requete selectionnée
    private void reqDispoValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_reqDispoValueChanged
        if (!requetesDispos.isEmpty()) {
            updateRequeteD();
        }
    }//GEN-LAST:event_reqDispoValueChanged

    //Change la catégorie
    private void catBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_catBoxItemStateChanged
        if (catBox.getSelectedIndex() > 0) {
            requetesAssignees.get(reqAssignee.getSelectedIndex()).setCategorie(Categorie.fromString((String) catBox.getSelectedItem()));
            updateRequeteA();
        }
    }//GEN-LAST:event_catBoxItemStateChanged

    //Assigne une requete au technicien à qui appartient cette Frame
    private void prendrereqBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prendrereqBtnActionPerformed
        if (reqDispo.getSelectedIndex() >= 0) {
            Requete r = requetesDispos.get(reqDispo.getSelectedIndex());
            r.setTech(loggedTechnicien);
            r.setStatut(Statut.enTraitement);

            requetesAssignees = loggedTechnicien.getRequetesAssignees();

            requetesAssigneesListModel = new DefaultListModel();
            requetesAssignees.forEach(requete -> requetesAssigneesListModel.addElement(requete.getSujet()));
            reqAssignee.setModel(requetesAssigneesListModel);

            requetesDisposListModel.remove(reqDispo.getSelectedIndex());
            requetesDispos.remove(r);

            updateTables();
            requeteSelectionnee = r;
            updateRequeteA();
            updateCommentaires();
            reqAssignee.setSelectedIndex(reqAssignee.getModel().getSize() - 1);
        }

    }//GEN-LAST:event_prendrereqBtnActionPerformed

    private void comInKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comInKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            requeteSelectionnee.addCommentaire(comIn.getText(), loggedTechnicien);
            String s = "";
            for (Commentaire cmt : requeteSelectionnee.getComments()) {
                s += cmt.toString();
            }
            commentsArea.setText(s);
            comIn.setText("");
        }
    }//GEN-LAST:event_comInKeyPressed

    private void ajoutfichierBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ajoutfichierBtnActionPerformed
        fileChooser.showOpenDialog(this);

        File f = fileChooser.getSelectedFile();
        if (f != null) {
            try {
                file = new File("dat/" + f.getName());
                InputStream srcFile = new FileInputStream(f);
                OutputStream newFile = new FileOutputStream(file);
                byte[] buf = new byte[4096];
                int len;
                while ((len = srcFile.read(buf)) > 0) {
                    newFile.write(buf, 0, len);
                }
                srcFile.close();
                newFile.close();
            } catch (IOException ex) {
                Logger.getLogger(NewRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (file.exists()) {
                FileAddedFrame ok = new FileAddedFrame();
                ok.setVisible(true);
            }
        }

        try {
            requetesAssignees.get(reqAssignee.getSelectedIndex()).setFile(file);
        } catch (IndexOutOfBoundsException e) {
        }

        updateRequeteA();
    }//GEN-LAST:event_ajoutfichierBtnActionPerformed

    private void fileLblMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fileLblMouseClicked
        try {
            Requete req = requetesAssignees.get(reqAssignee.getSelectedIndex());
            java.awt.Desktop dt = java.awt.Desktop.getDesktop();
            dt.open(req.getFichier());
        } catch (IOException ex) {
            Logger.getLogger(TechLoggedFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_fileLblMouseClicked

    private void finBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_finBoxItemStateChanged
        if (finBox.getSelectedIndex() > 0) {
            requetesAssignees.get(reqAssignee.getSelectedIndex()).setStatut(Statut.fromString((String) finBox.getSelectedItem()));
            updateRequeteA();
        }
    }//GEN-LAST:event_finBoxItemStateChanged

    private void assignerReqBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignerReqBtnActionPerformed
        if (reqDispo.getSelectedIndex() >= 0) {
            Requete r = requetesDispos.get(reqDispo.getSelectedIndex());
            Technicien autre = techniciens.get(supportList.getSelectedIndex());
            r.setTech(autre);
            r.setStatut(Statut.enTraitement);

            requetesDisposListModel.remove(reqDispo.getSelectedIndex());
            requetesDispos.remove(r);

            if (autre.getNomUtilisateur().equals(loggedTechnicien.getNomUtilisateur())) {
                requetesAssigneesListModel.addElement(r.getSujet());
            }

            updateTables();
        }
    }//GEN-LAST:event_assignerReqBtnActionPerformed

    private void affRapportBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_affRapportBtnActionPerformed
        String rap = "";
        for (Technicien tech : techniciens) {
            rap += tech.getNom() + ":\n";
            rap += tech.getRequeteParStatut() + "\n";
        }
        RapportFrame rapport = new RapportFrame(rap);
        rapport.setVisible(true);
    }//GEN-LAST:event_affRapportBtnActionPerformed

    private void initLayoutHorizontalGroup(javax.swing.GroupLayout layout) {
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(reqDispoLbl)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(32, 32, 32)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                        .addComponent(jScrollPane5)
                                                                        .addComponent(prendrereqBtn))
                                                                .addGap(18, 18, 18)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                        .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE)
                                                                        .addComponent(assignerReqBtn, javax.swing.GroupLayout.Alignment.LEADING))))
                                                .addGap(86, 86, 86))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                                                        .addComponent(reqAssignedLbl, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE))
                                                .addGap(18, 18, 18)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(reqSelLbl)
                                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(catBox, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(finBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(21, 21, 21)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(comLbl)
                                                                .addGroup(layout.createSequentialGroup()
                                                                        .addComponent(addComLbl)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(comIn)))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(ajoutfichierBtn)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(fileLbl)))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(quitBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(affRapportBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(addRequeteBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
        );

    }

    private void initLayoutVerticalGroup(javax.swing.GroupLayout layout) {
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(51, 51, 51)
                                                .addComponent(addRequeteBtn)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(affRapportBtn)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(quitBtn))
                                        .addGroup(layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(reqAssignedLbl)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                                        .addComponent(comLbl)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                .addComponent(addComLbl)
                                                                                .addComponent(comIn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                .addComponent(ajoutfichierBtn)
                                                                                .addComponent(fileLbl)))
                                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                                        .addComponent(reqSelLbl)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(catBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(finBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                                .addGap(3, 3, 3)))
                                .addGap(38, 38, 38)
                                .addComponent(reqDispoLbl)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(prendrereqBtn)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(assignerReqBtn)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)))
                                .addContainerGap())
        );
    }

    private void updateRequeteA() {
        if (reqAssignee.getSelectedIndex() >= 0) {
            requeteSelectionnee = requetesAssignees.get(reqAssignee.getSelectedIndex());
            requeteArea.setText(requeteSelectionnee.getDetails());
            String s = "";
            for (Commentaire cmt : requeteSelectionnee.getComments()) {
                s += cmt.toString();
            }
            commentsArea.setText(s);
            if (requeteSelectionnee.getFichier() != null && requeteSelectionnee.getFichier().exists()) {
                fileLbl.setVisible(true);
                fileLbl.setText("Afficher " + requeteSelectionnee.getFichier().getPath());
            } else {
                fileLbl.setVisible(false);
            }
        } else {
            requeteArea.setText("");
        }
        if (requeteSelectionnee.getStatut() == Statut.enTraitement) {
            finBox.setEnabled(true);
        } else {
            finBox.setEnabled(false);
        }
    }

    private void updateRequeteD() {
        if (reqDispo.getSelectedIndex() >= 0) {
            requeteSelectionnee = requetesDispos.get(reqDispo.getSelectedIndex());
            descReqArea.setText(requeteSelectionnee.getDetails());
        } else {
            descReqArea.setText("");
        }
    }

    private void updateCommentaires() {
        if (requeteSelectionnee != null) {
            if (!showComments) {
                String s = "";
                for (Commentaire c : requeteSelectionnee.getComments()) {
                    s += c.getUtilisateur().getNomUtilisateur() + ": " + c.getCommentaire() + "\n";

                }
                commentsArea.setText(s);
                showComments = true;
            }
        }
    }

    private void updateTables() {
        reqAssignee.setModel(requetesAssigneesListModel);
        if (!requetesDisposListModel.isEmpty()) {
            reqDispo.setModel(requetesDisposListModel);
            prendrereqBtn.setEnabled(true);
        } else {
            requetesDisposListModel.addElement("Il n'y a pas de requêtes pour le moment.");
            reqDispo.setModel(requetesDisposListModel);
            prendrereqBtn.setEnabled(false);
        }
    }

    public JButton getAddRequeteBtn() {
        return addRequeteBtn;
    }
    // End of variables declaration//GEN-END:variables


}
