package views;

import javax.swing.*;


public abstract class LoginFrame extends javax.swing.JFrame {

    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JLabel nomUtilisateurLbl;
    protected javax.swing.JLabel motDePasseLbl;
    protected javax.swing.JTextField nomUtilisateurInput;
    protected javax.swing.JTextField motDePasseInput;
    protected javax.swing.JLabel exNomUtilisateurLbl;
    protected javax.swing.JLabel exMotDePasseLbl;
    protected javax.swing.JButton loginBtn;
    protected javax.swing.JButton annulerBtn;
    private JFrame framePrecedent;

    public LoginFrame(JFrame framePrecedent) {
        this.framePrecedent = framePrecedent;
        initComponents();
        framePrecedent.setVisible(false);
        setVisible(true);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        nomUtilisateurLbl = new javax.swing.JLabel();
        motDePasseLbl = new javax.swing.JLabel();
        exNomUtilisateurLbl = new javax.swing.JLabel();
        exMotDePasseLbl = new javax.swing.JLabel();
        nomUtilisateurInput = new javax.swing.JTextField();
        motDePasseInput = new javax.swing.JTextField();
        loginBtn = new javax.swing.JButton();
        annulerBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        nomUtilisateurLbl.setText("Nom d'utilisateur:");
        motDePasseLbl.setText("Mot de passe:");
        exNomUtilisateurLbl.setText("ex.: Isabeau Desrochers");
        exMotDePasseLbl.setText("ex.: MonMdp");
        loginBtn.setText("OK");
        annulerBtn.setText("Annuler");
        annulerBtn.addActionListener(e -> onAnnulerBtnClick());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        initLayoutHorizontalGroup(layout);
        initLayoutVerticalGroup(layout);
        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void onAnnulerBtnClick() {//GEN-FIRST:event_onAnnulerBtnClick
        setVisible(false);
        framePrecedent.setVisible(true);
    }//GEN-LAST:event_onAnnulerBtnClick

    protected abstract void initLayoutHorizontalGroup(javax.swing.GroupLayout layout);

    protected abstract void initLayoutVerticalGroup(javax.swing.GroupLayout layout);

    public JButton getLoginBtn() {
        return loginBtn;
    }

    public JTextField getNomUtilisateurInput() {
        return nomUtilisateurInput;
    }

    public JTextField getMotDePasseInput() {
        return motDePasseInput;
    }
    // End of variables declaration//GEN-END:variables

}
