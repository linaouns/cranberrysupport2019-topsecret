package views;

import models.Commentaire;
import models.Requete;
import models.Statut;
import models.Utilisateur;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientLoggedFrame extends javax.swing.JFrame {

    private Utilisateur loggedClient;
    private Requete requeteSelectionnee;
    private File file;
    private boolean showComments = false;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addRequeteBtn;
    private javax.swing.JTextArea addCommentArea;
    private javax.swing.JButton addFichierBtn;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel modifRequeteLbl;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JList requetesCreesJList;
    private javax.swing.JButton addCommentBtn;
    private javax.swing.JButton quitBtn;
    private javax.swing.JTextArea requeteAndCommentDisplayArea;
    private javax.swing.JLabel requetesLbl;
    private javax.swing.JButton voirComsBtn;
    //
    private javax.swing.JButton requeteSatisfactionPositifBtn;
    private javax.swing.JButton requeteSatisfactionNegatifBtn;
    public ClientLoggedFrame(Utilisateur loggedClient) {
        this.loggedClient = loggedClient;
        initComponents();
        setVisible(true);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        fileChooser = new javax.swing.JFileChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        requetesCreesJList = new javax.swing.JList();
        requetesLbl = new javax.swing.JLabel();
        addRequeteBtn = new javax.swing.JButton();
        quitBtn = new javax.swing.JButton();
        addCommentBtn = new javax.swing.JButton();
        addFichierBtn = new javax.swing.JButton();
        modifRequeteLbl = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        requeteAndCommentDisplayArea = new javax.swing.JTextArea();
        voirComsBtn = new javax.swing.JButton();
        requeteSatisfactionNegatifBtn = new javax.swing.JButton();
        requeteSatisfactionPositifBtn = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        addCommentArea = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        initRequetesCreesJList();
        jScrollPane1.setViewportView(requetesCreesJList);

        requetesLbl.setText("Vos requêtes:");

        addRequeteBtn.setText("Faire une nouvelle requête");

        quitBtn.setText("Quitter");

        addCommentBtn.setText("Ajouter un commentaire");
        addCommentBtn.setEnabled(false);
        addCommentBtn.addActionListener(evt -> addCommentBtnActionPerformed(evt));

        addFichierBtn.setText("Ajouter un fichier");
        addFichierBtn.addActionListener(evt -> addFichierBtnActionPerformed(evt));

        modifRequeteLbl.setText("Modifier la requête sélectionnée:");

        requeteAndCommentDisplayArea.setColumns(20);
        requeteAndCommentDisplayArea.setEditable(false);
        requeteAndCommentDisplayArea.setLineWrap(true);
        requeteAndCommentDisplayArea.setRows(5);
        jScrollPane2.setViewportView(requeteAndCommentDisplayArea);

        voirComsBtn.setBackground(new java.awt.Color(102, 153, 255));
        voirComsBtn.setText("Voir les commentaires");
        voirComsBtn.addActionListener(evt -> voirComsBtnActionPerformed(evt));

        requeteSatisfactionNegatifBtn.setBackground(new java.awt.Color(255, 100, 100));
        requeteSatisfactionNegatifBtn.setText("Insatisfait");
        requeteSatisfactionNegatifBtn.addActionListener(evt -> requeteSatisfactionNegatifBtnActionPerformed(evt));
        requeteSatisfactionNegatifBtn.setEnabled(false);

        requeteSatisfactionPositifBtn.setBackground(new java.awt.Color(100, 255, 150));
        requeteSatisfactionPositifBtn.setText("Satisfait");
        requeteSatisfactionPositifBtn.addActionListener(evt -> requeteSatisfactionPositifBtnActionPerformed(evt));
        requeteSatisfactionPositifBtn.setEnabled(false);

        addCommentArea.setColumns(20);
        addCommentArea.setLineWrap(true);
        addCommentArea.setRows(5);
        addCommentArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                addCommentAreaKeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(addCommentArea);
        addCommentArea.setVisible(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        initLayoutHorizontalGroup(layout);
        initLayoutVerticalGroup(layout);
        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void initRequetesCreesJList() {
        DefaultListModel requetesListModel = new DefaultListModel();
        if (loggedClient.getRequetesCreees().isEmpty()) {
            requetesListModel.addElement("Vous n'avez pas de requête encore.");
        } else {
            for (Requete requete : loggedClient.getRequetesCreees()) {
                requetesListModel.addElement(requete.getSujet());
            }
        }
        requetesCreesJList.setModel(requetesListModel);
        requetesCreesJList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                onSelectRequeteInJList(evt);
            }
        });
    }

    private void onSelectRequeteInJList(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listRequeteListValueChanged
        displayRequeteSelectionnee();
    }//GEN-LAST:event_listRequeteListValueChanged

    private void addFichierBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fichierBtnActionPerformed
        fileChooser.showOpenDialog(this);
        file = fileChooser.getSelectedFile();
        try {
            loggedClient.getRequetesCreees().get(requetesCreesJList.getSelectedIndex()).setFile(file);
        } catch (IndexOutOfBoundsException e) {
        }

        File f = fileChooser.getSelectedFile();
        if (f != null) {
            try {
                new FileWriter("dat/" + f.getName());
            } catch (IOException ex) {
                Logger.getLogger(NewRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (file.exists()) {
                FileAddedFrame ok = new FileAddedFrame();
                ok.setVisible(true);
            }
        }
    }//GEN-LAST:event_fichierBtnActionPerformed

    private void addCommentBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifBtnActionPerformed
        addCommentArea.setVisible(true);
        addCommentArea.requestFocus();
    }//GEN-LAST:event_modifBtnActionPerformed

    private void voirComsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_voirComsBtnActionPerformed
        if (requeteSelectionnee != null) {
            if (!showComments) {
                String s = "";
                for (Commentaire c : requeteSelectionnee.getComments()) {
                    s += c.toString();
                }
                requeteAndCommentDisplayArea.setText(s);
                showComments = true;
                voirComsBtn.setText("Voir la rêquete");
                addFichierBtn.setEnabled(false);
                addCommentBtn.setEnabled(true);
            } else {
                displayRequeteSelectionnee();
            }
        }
    }//GEN-LAST:event_voirComsBtnActionPerformed

    private void requeteSatisfactionNegatifBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_requeteSatisfactionNegatifBtnActionPerformed
        requeteSelectionnee.setSatisfaction(false);
        requeteSatisfactionPositifBtn.setEnabled(false);
        displayRequeteSelectionnee();
    }//GEN-LAST:event_requeteSatisfactionNegatifBtnActionPerformed

    private void requeteSatisfactionPositifBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_requeteSatisfactionPositifBtnActionPerformed
        requeteSelectionnee.setSatisfaction(true);
        requeteSatisfactionNegatifBtn.setEnabled(false);
        displayRequeteSelectionnee();
    }//GEN-LAST:event_requeteSatisfactionPositifBtnActionPerformed

    private void displayRequeteSelectionnee() {
        requeteSelectionnee = loggedClient.getRequetesCreees().get(requetesCreesJList.getSelectedIndex());
        requeteAndCommentDisplayArea.setText(requeteSelectionnee.getDetails());
        showComments = false;
        voirComsBtn.setText("Voir les commentaires");
        addFichierBtn.setEnabled(true);
        addCommentBtn.setEnabled(false);
        if (requeteSelectionnee.getSatisfaction() == null && (requeteSelectionnee.getStatut() == Statut.finalSucces || requeteSelectionnee.getStatut() == Statut.finalAbandon)) {
            requeteSatisfactionNegatifBtn.setEnabled(true);
            requeteSatisfactionPositifBtn.setEnabled(true);
        } else {
            requeteSatisfactionNegatifBtn.setEnabled(false);
            requeteSatisfactionPositifBtn.setEnabled(false);
        }
    }

    private void addCommentAreaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_commentAreaKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            requeteSelectionnee.addCommentaire(addCommentArea.getText(), loggedClient);
            String s = "";
            for (Commentaire c : requeteSelectionnee.getComments()) {
                s += c.toString();
            }
            requeteAndCommentDisplayArea.setText(s);
            addCommentArea.setVisible(false);
            addCommentBtn.requestFocus();
        }
    }//GEN-LAST:event_commentAreaKeyPressed

    private void initLayoutHorizontalGroup(javax.swing.GroupLayout layout) {
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(addRequeteBtn)
                                        .addComponent(requetesLbl)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(quitBtn))
                                                .addGap(10, 10, 10)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(voirComsBtn)
                                                        .addComponent(requeteSatisfactionPositifBtn)
                                                        .addComponent(requeteSatisfactionNegatifBtn)
                                                        .addComponent(addFichierBtn)
                                                        .addComponent(addCommentBtn)
                                                        .addComponent(modifRequeteLbl)
                                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(9, 9, 9)
                                                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)))
                                .addContainerGap())
        );
    }

    private void initLayoutVerticalGroup(javax.swing.GroupLayout layout) {
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                .addGap(76, 76, 76)
                                                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(22, 22, 22)
                                                .addComponent(addRequeteBtn)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(requetesLbl)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(quitBtn))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(voirComsBtn)
                                                                .addGap(26, 26, 26)
                                                                .addComponent(requeteSatisfactionPositifBtn)
                                                                .addComponent(requeteSatisfactionNegatifBtn)
                                                                .addGap(26, 26, 26)
                                                                .addComponent(modifRequeteLbl)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(addFichierBtn)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(addCommentBtn)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)))))
                                .addContainerGap())
        );

    }

    public JButton getAddRequeteBtn() {
        return addRequeteBtn;
    }

    public JButton getQuitBtn() {
        return quitBtn;
    }
    // End of variables declaration//GEN-END:variables
}
