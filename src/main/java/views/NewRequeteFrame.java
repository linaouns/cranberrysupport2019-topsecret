package views;

import javax.swing.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * views.newRequeteFrame.java
 *
 */

/**
 * @author Anonyme
 */
public class NewRequeteFrame extends javax.swing.JFrame {

    private JFrame framePrecedent;
    private File file;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox catBox;
    private javax.swing.JLabel catLbl;
    private javax.swing.JTextArea descpArea;
    private javax.swing.JScrollPane descpScroll;
    private javax.swing.JLabel descripLbl;
    private javax.swing.JButton creerRequeteBtn;
    private javax.swing.JLabel fichierLbl;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField pathFld;
    private javax.swing.JButton quitBtn;
    private javax.swing.JButton fileUploadBtn;
    private javax.swing.JTextField sujetFld;
    private javax.swing.JLabel sujetLbl;
    private javax.swing.JLabel titleLbl;
    public NewRequeteFrame(JFrame framePrecedent) {
        this.framePrecedent = framePrecedent;
        framePrecedent.setVisible(false);
        initComponents();
        this.setVisible(true);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        fileChooser = new javax.swing.JFileChooser();
        titleLbl = new javax.swing.JLabel();
        sujetLbl = new javax.swing.JLabel();
        sujetFld = new javax.swing.JTextField();
        descripLbl = new javax.swing.JLabel();
        descpScroll = new javax.swing.JScrollPane();
        descpArea = new javax.swing.JTextArea();
        fichierLbl = new javax.swing.JLabel();
        pathFld = new javax.swing.JTextField();
        fileUploadBtn = new javax.swing.JButton();
        catBox = new javax.swing.JComboBox();
        catLbl = new javax.swing.JLabel();
        creerRequeteBtn = new javax.swing.JButton();
        quitBtn = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        titleLbl.setText("Nouvelle requête");

        sujetLbl.setText("Sujet de la requête:");

        descripLbl.setText("Description:");

        descpArea.setColumns(20);
        descpArea.setRows(5);
        descpArea.setBorder(null);
        descpScroll.setViewportView(descpArea);

        fichierLbl.setText("Fichier:");

        pathFld.addActionListener(evt -> pathFldActionPerformed(evt));

        fileUploadBtn.setText("Téléverser un fichier");
        fileUploadBtn.addActionListener(evt -> fileUploadBtnActionPerformed(evt));

        catBox.setModel(new javax.swing.DefaultComboBoxModel(
                new String[]{"Poste de travail", "Serveur", "Service web", "Compte usager", "Autre"}
        ));

        catLbl.setText("Catégorie:");

        creerRequeteBtn.setText("Terminer");

        quitBtn.setText("Revenir");
        quitBtn.addActionListener(evt -> quitBtnActionPerformed(evt));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        initLayoutHorizontalGroup(layout);
        initLayoutVerticalGroup(layout);
        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void initLayoutHorizontalGroup(javax.swing.GroupLayout layout) {
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(titleLbl)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(sujetLbl)
                                                                        .addComponent(descripLbl)
                                                                        .addComponent(fichierLbl))
                                                                .addGap(18, 18, 18)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(pathFld, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                                                                        .addComponent(sujetFld, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                                                                        .addComponent(descpScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                                                                        .addComponent(fileUploadBtn))))
                                                .addContainerGap(30, Short.MAX_VALUE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(catLbl)
                                                .addGap(63, 63, 63)
                                                .addComponent(catBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addContainerGap(176, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGap(124, 124, 124)
                                                .addComponent(creerRequeteBtn)
                                                .addGap(18, 18, 18)
                                                .addComponent(quitBtn)
                                                .addContainerGap(104, Short.MAX_VALUE))))
                        .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
    }

    private void initLayoutVerticalGroup(javax.swing.GroupLayout layout) {
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(titleLbl)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(sujetLbl)
                                        .addComponent(sujetFld, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(descripLbl)
                                        .addComponent(descpScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(pathFld, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(fichierLbl))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(fileUploadBtn)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(catBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(catLbl))
                                .addGap(27, 27, 27)
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(quitBtn)
                                        .addComponent(creerRequeteBtn))
                                .addContainerGap(31, Short.MAX_VALUE))
        );
    }

    private void pathFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pathFldActionPerformed
        pathFld.setText(fileChooser.getSelectedFile().getPath());
    }//GEN-LAST:event_pathFldActionPerformed

    private void fileUploadBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selPathBtnActionPerformed
        fileChooser.showOpenDialog(framePrecedent);

        pathFldActionPerformed(evt);
        try {
            file = new File("dat/" + fileChooser.getSelectedFile().getName());
            InputStream srcFile = new FileInputStream(fileChooser.getSelectedFile());
            OutputStream newFile = new FileOutputStream(file);
            byte[] buf = new byte[4096];
            int len;
            while ((len = srcFile.read(buf)) > 0) {
                newFile.write(buf, 0, len);
            }
            srcFile.close();
            newFile.close();
        } catch (IOException ex) {
            Logger.getLogger(NewRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_selPathBtnActionPerformed

    private void quitBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitBtnActionPerformed
        this.setVisible(false);
        framePrecedent.setVisible(true);
    }//GEN-LAST:event_quitBtnActionPerformed

    public JButton getCreerRequeteBtn() {
        return creerRequeteBtn;
    }

    public File getFile() {
        return file;
    }

    public JComboBox getCatBox() {
        return catBox;
    }

    public JTextArea getDescpArea() {
        return descpArea;
    }

    public JTextField getSujetFld() {
        return sujetFld;
    }
    // End of variables declaration//GEN-END:variables
}
