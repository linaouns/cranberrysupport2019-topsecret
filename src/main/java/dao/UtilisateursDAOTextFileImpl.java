package dao;

import models.Technicien;
import models.Utilisateur;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UtilisateursDAOTextFileImpl implements UtilisateursDAO {

    private static UtilisateursDAOTextFileImpl instance;
    private static Scanner scanner;
    private List<Utilisateur> utilisateurs;

    private UtilisateursDAOTextFileImpl() {
        utilisateurs = new ArrayList<>();
    }

    public static UtilisateursDAOTextFileImpl getInstance() {
        if (instance == null) {
            instance = new UtilisateursDAOTextFileImpl();
            instance.loadUtilisateurs();
        }
        return instance;
    }

    public static UtilisateursDAOTextFileImpl getInstance(Scanner pScanner) {
        scanner = pScanner;
        instance = null;
        return getInstance();
    }

    private void loadUtilisateurs() {
        try {
            if (scanner == null) {
                scanner = new Scanner(new FileReader("dat/BanqueUtilisateur.txt"));
            }
            while (scanner.hasNextLine()) {
                String s = scanner.nextLine();
                String[] ss = s.split(";");
                addUtilisateur(ss[0], ss[1], ss[2], ss[3], ss[4]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addUtilisateur(String prenom, String nom, String nomUtilisateur, String mdp, String role) {
        utilisateurs.add(
                role.equals(Utilisateur.Role.client.toString())
                        ? new Utilisateur(prenom, nom, nomUtilisateur, mdp)
                        : new Technicien(prenom, nom, nomUtilisateur, mdp)
        );
    }

    @Override
    public List<Utilisateur> getAll() {
        return utilisateurs;
    }


    @Override
    public int save(List<Utilisateur> utilisateurs) {
        return 0;
    }
}
