package dao;

import driver.H2Driver;
import models.Technicien;
import models.Utilisateur;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UtilisateursDAOH2Impl implements UtilisateursDAO {
    private static final String CREATE_TABLE_UTILISATEUR = "CREATE TABLE IF NOT EXISTS Utilisateur(id INT AUTO_INCREMENT PRIMARY KEY, " +
            "prenom VARCHAR(255), nom VARCHAR(255), username VARCHAR(255), password VARCHAR(255), role VARCHAR(100));";
    private static final String SQL_INSERT_UTILISATEUR = "INSERT INTO Utilisateur(prenom, nom, username, password, " +
            "role) VALUES(?, ?, ?, ?, ?)";
    private static final String SQL_SELECT_UTILISATEUR = "SELECT * FROM Utilisateur";
    private static int HAS_NOT_BEEN_ADDED = -99;
    private static UtilisateursDAOH2Impl instance = null;
    private static H2Driver driver;
    private static PreparedStatement psInsertUtilisateur;
    private List<Utilisateur> utilisateurs;

    private UtilisateursDAOH2Impl() {
        utilisateurs = new ArrayList<>();
    }

    public static UtilisateursDAOH2Impl getInstance() throws Exception {
        if (instance == null) {
            instance = new UtilisateursDAOH2Impl();
            instance.initializeDatabaseEnvironment();
        }
        return instance;
    }

    private void initializeDatabaseEnvironment() throws Exception {
        if (driver == null)
            driver = H2Driver.getInstance();
        driver.getStatement().executeUpdate(CREATE_TABLE_UTILISATEUR);
        psInsertUtilisateur = driver.getConnection().prepareStatement(SQL_INSERT_UTILISATEUR, Statement.RETURN_GENERATED_KEYS);
    }

    @Override
    public List<Utilisateur> getAll() {
        utilisateurs = new ArrayList<>();
        try {
            ResultSet resultSet = driver.getStatement().executeQuery(SQL_SELECT_UTILISATEUR);
            while (resultSet.next()) {
                utilisateurs.add(retrieveUtilisateurFromResultSet(resultSet));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return utilisateurs;
    }

    private Utilisateur retrieveUtilisateurFromResultSet(ResultSet resultSet) throws SQLException {
        String prenom = resultSet.getString(2);
        String nom = resultSet.getString(3);
        String username = resultSet.getString(4);
        String motDePasse = resultSet.getString(5);
        String role = resultSet.getString(6);

        return role.equals(Utilisateur.Role.client.toString())
                ? new Utilisateur(prenom, nom, username, motDePasse)
                : new Technicien(prenom, nom, username, motDePasse);
    }

    @Override
    public int save(List<Utilisateur> utilisateursToPersist) {
        int nbUtilisateursSaved = 0;
        try {
            clearDb();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Utilisateur utilisateur : utilisateursToPersist) {
            if (save(utilisateur))
                nbUtilisateursSaved++;
        }
        try {
            driver.persistData();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nbUtilisateursSaved;
    }

    private void clearDb() throws SQLException {
        driver.getStatement().executeUpdate("DELETE FROM Utilisateur;;");
    }

    private boolean save(Utilisateur utilisateur) {
        int addedRequeteId = HAS_NOT_BEEN_ADDED;
        if (psInsertUtilisateur != null) {
            try {
                setUpAndExecuteInsertUtilisateurPreparedStatement(utilisateur);
                ResultSet generatedKeys = psInsertUtilisateur.getGeneratedKeys();
                if (generatedKeys.next()) {
                    addedRequeteId = generatedKeys.getInt(1);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return addedRequeteId != HAS_NOT_BEEN_ADDED;
    }

    private void setUpAndExecuteInsertUtilisateurPreparedStatement(Utilisateur utilisateur) throws SQLException {
        psInsertUtilisateur.setString(1, utilisateur.getPrenom());
        psInsertUtilisateur.setString(2, utilisateur.getNom());
        psInsertUtilisateur.setString(3, utilisateur.getNomUtilisateur());
        psInsertUtilisateur.setString(4, utilisateur.getMotDePasse());
        psInsertUtilisateur.setString(5, utilisateur.getRole());
        psInsertUtilisateur.executeUpdate();
    }
}
