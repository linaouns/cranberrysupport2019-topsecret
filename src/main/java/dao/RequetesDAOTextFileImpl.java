package dao;

import models.Categorie;
import models.Requete;
import models.Statut;
import services.BanqueUtilisateurs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class RequetesDAOTextFileImpl implements RequetesDAO {

    private static RequetesDAOTextFileImpl instance = null;
    private static Scanner scanner;
    private static BufferedWriter bufferedWriter;
    private static BanqueUtilisateurs banqueUtilisateurs;

    private List<Requete> requetes;

    private RequetesDAOTextFileImpl() {
        requetes = new ArrayList<>();
    }

    public static RequetesDAOTextFileImpl getInstance() {
        if (instance == null) {
            instance = new RequetesDAOTextFileImpl();
            instance.loadRequetes();
        }
        return instance;
    }

    public static RequetesDAOTextFileImpl getInstance(Scanner pScanner, BufferedWriter pBufferedWriter,
                                                      BanqueUtilisateurs pBanqueUtilisateurs) {
        banqueUtilisateurs = pBanqueUtilisateurs;
        scanner = pScanner;
        bufferedWriter = pBufferedWriter;
        instance = null;
        return getInstance();
    }

    private void loadRequetes() {
        try {
            if (scanner == null)
                scanner = new Scanner(new File("dat/BanqueRequetes.txt"));
            while (scanner.hasNextLine()) {
                String s = scanner.nextLine();
                if (!s.isEmpty()) {
                    String[] ss = s.split(";~", -1);
                    requetes.add(newRequeteString(ss[0], ss[1], ss[2], ss[3], ss[4], ss[5], ss[6], ss[7], ss[8]));
                }
            }
            scanner.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Requete> getAll() {
        return requetes;
    }

    private Requete newRequeteString(String sujet, String descrip, String client,
                                     String categorie, String statut, String satisfaction, String path, String tech, String commentaires) {

        if (banqueUtilisateurs == null)
            banqueUtilisateurs = BanqueUtilisateurs.getUserInstance();

        Requete requete = new Requete(sujet, descrip,
                banqueUtilisateurs.chercheUtilisateurParUserName(client),
                Categorie.fromString(categorie));

        if (!satisfaction.isEmpty()) {
            requete.setSatisfaction(Boolean.valueOf(satisfaction));
        }

        requete.setStatut(Statut.fromString(statut));
        File tempo = new File(path);
        requete.setFile(tempo);
        requete.setTech(banqueUtilisateurs.chercheUtilisateurParUserName(tech));

        parseAndSetCommentairesToRequete(commentaires, requete);

        requete.getClient().ajouterRequeteCreee(requete);
        return requete;
    }

    private void parseAndSetCommentairesToRequete(String commentaires, Requete requete) {
        if (!commentaires.equals("")) {
            String[] comms = commentaires.split("/%");
            for (int i = 0; i < comms.length; i++) {
                String[] ceCommentaire = comms[i].split("&#");
                requete.addCommentaire(ceCommentaire[0],
                        banqueUtilisateurs.chercheUtilisateurParUserName(ceCommentaire[1]));
            }
        }
    }

    @Override
    public int save(List<Requete> requetes) {
        int nbRequetesSaved = 0;
        try {
            if (bufferedWriter == null) {
                bufferedWriter = new BufferedWriter(new FileWriter("dat/BanqueRequetes.txt"));
            }
            String requetesToString = requetes.stream().map(requete -> requete.toString())
                    .collect(Collectors.joining("\n")) + "\n";

            bufferedWriter.write(requetesToString);
            nbRequetesSaved = requetes.size();
            bufferedWriter.close();
        } catch (IOException e) {
        }

        return nbRequetesSaved;
    }
}
