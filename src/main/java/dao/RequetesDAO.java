package dao;

import models.Requete;

import java.util.List;

public interface RequetesDAO {
    List<Requete> getAll();

    int save(List<Requete> requetes);
}
