package dao;

import models.Utilisateur;

import java.util.List;

public interface UtilisateursDAO {
    List<Utilisateur> getAll();

    int save(List<Utilisateur> utilisateurs);
}
