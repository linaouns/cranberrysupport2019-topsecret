package dao;

import driver.H2Driver;
import models.*;
import services.BanqueUtilisateurs;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class RequetesDAOH2Impl implements RequetesDAO {
    private static final String CREATE_TABLE_REQUETE = "CREATE TABLE IF NOT EXISTS Requete(id INT AUTO_INCREMENT PRIMARY KEY, sujet VARCHAR(255), " +
            "description VARCHAR(255), auteur VARCHAR(255), categorie VARCHAR(255), statut VARCHAR(100), " +
            "file_path VARCHAR(255), tech_assigne VARCHAR(255), satisfaction VARCHAR(10));";
    private static final String CREATE_TABLE_COMMENTAIRE = "CREATE TABLE IF NOT EXISTS Commentaire(id INT AUTO_INCREMENT PRIMARY KEY, " +
            "commentaire VARCHAR(255), auteur VARCHAR(255), requete_id INT);";
    private static final String SQL_INSERT_REQUETE = "INSERT INTO Requete(sujet, description, auteur, categorie, statut, file_path, tech_assigne, satisfaction) " +
            "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_INSERT_COMMENTAIRE = "INSERT INTO Commentaire(commentaire, auteur, requete_id) VALUES(?, ?, ?)";
    private static final String SQL_SELECT_REQUETE = "SELECT * FROM Requete";
    private static final String SQL_SELECT_COMMENTAIRE = "SELECT commentaire, auteur FROM Commentaire WHERE requete_id = ?";
    private static int HAS_NOT_BEEN_ADDED = -99;
    private static RequetesDAOH2Impl instance = null;
    private static H2Driver driver;
    private static PreparedStatement psInsertRequete;
    private static PreparedStatement psInsertCommentaire;
    private static PreparedStatement psSelectCommentaire;
    private List<Requete> requetes;

    private RequetesDAOH2Impl() {
        requetes = new ArrayList<>();
    }

    public static RequetesDAOH2Impl getInstance() throws Exception {
        if (instance == null) {
            instance = new RequetesDAOH2Impl();
            instance.initializeDatabaseEnvironment();
        }
        return instance;
    }

    private void initializeDatabaseEnvironment() throws Exception {
        if (driver == null)
            driver = H2Driver.getInstance();
        driver.getStatement().executeUpdate(CREATE_TABLE_REQUETE);
        driver.getStatement().executeUpdate(CREATE_TABLE_COMMENTAIRE);
        psInsertRequete = driver.getConnection().prepareStatement(SQL_INSERT_REQUETE, Statement.RETURN_GENERATED_KEYS);
        psInsertCommentaire = driver.getConnection().prepareStatement(SQL_INSERT_COMMENTAIRE, Statement.RETURN_GENERATED_KEYS);
        psSelectCommentaire = driver.getConnection().prepareStatement(SQL_SELECT_COMMENTAIRE);
    }

    @Override
    public List<Requete> getAll() {
        requetes = new ArrayList<>();
        try {
            ResultSet resultSet = driver.getStatement().executeQuery(SQL_SELECT_REQUETE);
            while (resultSet.next()) {
                int requeteId = resultSet.getInt(1);
                Requete requete = retrieveRequeteFromResultSet(resultSet);
                setRequeteCommentaires(requeteId, requete);
                requetes.add(requete);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return requetes;
    }

    private Requete retrieveRequeteFromResultSet(ResultSet resultSet) throws SQLException {
        String sujet = resultSet.getString(2);
        String description = resultSet.getString(3);
        String username = resultSet.getString(4);
        String categorieString = resultSet.getString(5);
        String statutString = resultSet.getString(6);
        String filePath = resultSet.getString(7);
        String techUsername = resultSet.getString(8);
        String satisfaction = resultSet.getString(9);

        Utilisateur auteur = BanqueUtilisateurs.getUserInstance().chercheUtilisateurParUserName(username);
        Categorie categorie = Categorie.fromString(categorieString);
        Statut statut = Statut.fromString(statutString);
        File file = filePath != null ? new File(filePath) : null;
        Utilisateur technicien = BanqueUtilisateurs.getUserInstance().chercheUtilisateurParUserName(techUsername);

        Requete requete = new Requete(sujet, description, auteur, categorie);
        requete.setStatut(statut);
        requete.setFile(file);
        requete.setTech(technicien);
        if (requete.getClient() != null)
            requete.getClient().ajouterRequeteCreee(requete);
        if (satisfaction.equals("true") || satisfaction.equals("false"))
            requete.setSatisfaction(Boolean.valueOf(satisfaction));
        return requete;
    }

    private void setRequeteCommentaires(int requeteId, Requete requete) throws SQLException {
        psSelectCommentaire.setInt(1, requeteId);
        ResultSet resultSet = psSelectCommentaire.executeQuery();

        while (resultSet.next()) {
            requete.addCommentaire(
                    resultSet.getString(1),
                    BanqueUtilisateurs.getUserInstance().chercheUtilisateurParUserName(resultSet.getString(2))
            );
        }
    }

    @Override
    public int save(List<Requete> requetesToPersist) {
        int nbRequetesSaved = 0;
        try {
            clearDb();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Requete requete : requetesToPersist) {
            if (save(requete))
                nbRequetesSaved++;
        }
        try {
            driver.persistData();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nbRequetesSaved;
    }

    private void clearDb() throws SQLException {
        driver.getStatement().executeUpdate("DELETE FROM Requete;");
        driver.getStatement().executeUpdate("DELETE FROM Commentaire;");
    }

    private boolean save(Requete requete) {
        int addedRequeteId = HAS_NOT_BEEN_ADDED;
        if (psInsertRequete != null) {
            try {
                setUpAndExecuteInsertRequetePreparedStatement(requete);
                ResultSet generatedKeys = psInsertRequete.getGeneratedKeys();
                if (generatedKeys.next()) {
                    addedRequeteId = generatedKeys.getInt(1);
                }
                if (addedRequeteId != HAS_NOT_BEEN_ADDED) {
                    saveCommentaires(requete, addedRequeteId);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return addedRequeteId != HAS_NOT_BEEN_ADDED;
    }

    private void setUpAndExecuteInsertRequetePreparedStatement(Requete requete) throws SQLException {
        psInsertRequete.setString(1, requete.getSujet());
        psInsertRequete.setString(2, requete.getDescription());
        psInsertRequete.setString(3, requete.getClient().getNomUtilisateur());
        psInsertRequete.setString(4, requete.getCategorie().getValueString());
        psInsertRequete.setString(5, requete.getStatut().getValueString());
        psInsertRequete.setString(6, (requete.getFichier() != null ? requete.getFichier().getPath() : null));
        psInsertRequete.setString(7, (requete.getTech() != null ? requete.getTech().getNomUtilisateur() : null));
        psInsertRequete.setString(8, String.valueOf(requete.getSatisfaction()));
        psInsertRequete.executeUpdate();
    }

    private void saveCommentaires(Requete requete, int requeteId) {
        if (psInsertCommentaire != null) {
            for (Commentaire commentaire : requete.getComments()) {
                try {
                    psInsertCommentaire.setString(1, commentaire.getCommentaire());
                    psInsertCommentaire.setString(2, commentaire.getUtilisateur().getNomUtilisateur());
                    psInsertCommentaire.setInt(3, requeteId);
                    psInsertCommentaire.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
