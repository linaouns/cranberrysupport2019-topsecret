package models;

public enum Statut {

    ouvert("ouvert"),
    enTraitement("en traitement"),
    finalAbandon("Abandon"),
    finalSucces("Succès");

    String valeur;

    Statut(String s) {
        this.valeur = s;
    }

    public static Statut fromString(String text) {
        for (Statut statut : Statut.values()) {
            if (statut.getValueString().equals(text))
                return statut;
        }
        return null;
    }

    public String getValueString() {
        return valeur;
    }
}