package models;

public enum Categorie {
    posteDeTravail("Poste de travail"),
    serveur("Serveur"),
    serviceWeb("Service web"),
    compteUsager("Compte usager"),
    autre("Autre");

    String categorie;

    Categorie(String s) {
        this.categorie = s;
    }

    public static Categorie fromString(String text) {
        for (Categorie categorie : Categorie.values()) {
            if (categorie.getValueString().equals(text))
                return categorie;
        }
        return null;
    }

    public String getValueString() {
        return categorie;
    }
}