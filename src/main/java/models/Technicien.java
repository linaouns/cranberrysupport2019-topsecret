package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Technicien extends Utilisateur {

    private List<Requete> requetesAssignees;

    public Technicien(String prenom, String nom, String nomUtilisateur, String mdp) {
        super(prenom, nom, nomUtilisateur, mdp);
        role = Role.technicien.toString();
        requetesAssignees = new ArrayList<>();
    }

    public List<Requete> getRequetesAssignees() {
        return requetesAssignees;
    }

    public void assignerRequete(Requete requete) {
        requetesAssignees.add(requete);
    }

    public String getRequeteParStatut() {
        Map<String, Integer> requetesCompteByStatut = getRequetesCompteByStatut();

        return "Statut ouvert: " + getRequetesCreees().stream().filter(req -> req.getStatut().equals(Statut.ouvert)).count() + "\n"
                + "Statut En traitement: " + getCompteByStatut(requetesCompteByStatut, Statut.enTraitement) + "\n"
                + "Statut succès: " + getCompteByStatut(requetesCompteByStatut, Statut.finalSucces) + "\n"
                + "Statut abandonnée: " + getCompteByStatut(requetesCompteByStatut, Statut.finalAbandon) + "\n";
    }

    private Map<String, Integer> getRequetesCompteByStatut() {
        Map<String, Integer> requetesCompteByStatut = new HashMap<>();
        requetesAssignees.forEach(req -> {
            String statut = req.getStatut().getValueString();
            int compte = requetesCompteByStatut.get(statut) == null ? 1 : (requetesCompteByStatut.get(statut) + 1);
            requetesCompteByStatut.put(statut, compte);
        });
        return requetesCompteByStatut;
    }

    private int getCompteByStatut(Map<String, Integer> requetesCompteByStatut, Statut statut) {
        return (requetesCompteByStatut.get(statut.getValueString()) == null ? 0 : requetesCompteByStatut.get(statut.getValueString()));
    }

    @Override
    public void closeAccount() {
        super.closeAccount();
        getRequetesAssignees().forEach(requete -> requete.unAssignRequest());
        getRequetesAssignees().clear();
    }
}