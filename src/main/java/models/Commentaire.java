package models;

public class Commentaire {

    private String commentaire;
    private Utilisateur utilisateur;

    public Commentaire(String commentaire, Utilisateur utilisateur) {
        this.commentaire = commentaire;
        this.utilisateur = utilisateur;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public String toString() {
        return getUtilisateur().getNomUtilisateur() + ": " + getCommentaire() + "\n";
    }
}