package models;

import java.util.ArrayList;
import java.util.List;

public class Utilisateur {

    protected String prenom;
    protected String nom;
    protected String nomUtilisateur;
    protected String motDePasse;
    protected String role;
    protected boolean accountLocked;
    protected List<Requete> requetesCreees;

    public Utilisateur(String prenom, String nom, String nomUtilisateur, String motDePasse) {
        this.prenom = prenom;
        this.nom = nom;
        this.nomUtilisateur = nomUtilisateur;
        this.motDePasse = motDePasse;
        role = Role.client.toString();
        requetesCreees = new ArrayList<>();
    }

    public Utilisateur(String prenom, String nom, String nomUtilisateur, String motDePasse, String type) {
        this.prenom = prenom;
        this.nom = nom;
        this.nomUtilisateur = nomUtilisateur;
        this.motDePasse = motDePasse;
        requetesCreees = new ArrayList<>();
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public String getNomUtilisateur() {
        return nomUtilisateur;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public String getRole() {
        return role;
    }

    public List<Requete> getRequetesCreees() {
        return requetesCreees;
    }

    public void ajouterRequeteCreee(Requete requete) {
        requetesCreees.add(requete);
    }

    public boolean login(String nomUtilisateur, String motDePasse) {
        return (this.nomUtilisateur.equalsIgnoreCase(nomUtilisateur) && this.motDePasse.equals(motDePasse));
    }

    public boolean isLocked() {
        return accountLocked;
    }

    public void closeAccount() {
        accountLocked = true;
    }

    @Override
    public String toString() {
        return prenom + ';' + nom + ';' + nomUtilisateur + ';' + motDePasse + ';' + role;
    }

    public enum Role {
        technicien, client, administrateur;
    }
}