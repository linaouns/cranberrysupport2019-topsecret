package models;

import java.io.File;
import java.util.ArrayList;


public class Requete {
    private String sujet;
    private String description;
    private File fichier;
    private Integer numero;
    private Utilisateur client;
    private Technicien technicien;
    private Statut statut;
    private Categorie categorie;
    private Boolean satisfaction;
    private ArrayList<Commentaire> listeCommentaires;

    public Requete(String sujet, String description, Utilisateur client, Categorie categorie) {
        this.sujet = sujet;
        this.description = description;
        this.client = client;
        this.categorie = categorie;

        statut = Statut.ouvert;
        listeCommentaires = new ArrayList<Commentaire>();
    }

    public Boolean getSatisfaction() {
        if (satisfaction == null) {
            return null;
        }
        return satisfaction;
    }

    public void setSatisfaction(boolean satisfaction) {
        this.satisfaction = satisfaction;
    }

    public void unAssignRequest() {
        this.technicien = null;
    }

    public void setFile(File file) {
        fichier = file;
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String nouveau) {
        sujet = nouveau;
    }

    public File getFichier() {
        return fichier;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut newstat) {
        statut = newstat;
    }

    public ArrayList<Commentaire> getComments() {
        return listeCommentaires;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie choix) {
        categorie = choix;
    }

    public Technicien getTech() {
        return technicien;
    }

    public void setTech(Utilisateur tech) {
        if (tech != null && tech.getRole().equals(Utilisateur.Role.technicien.toString())) {
            this.technicien = (Technicien) tech;
            technicien.assignerRequete(this);
        }
    }

    public Utilisateur getClient() {
        return client;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String nouvelle) {
        description = nouvelle;
    }

    public String getDetails() {
        return ("Sujet: " + sujet
                + "\nDescription: " + description
                + "\nCatégorie: " + categorie.toString()
                + "\nStatut: " + statut.toString()
                + (technicien != null ? "\nTechnicien assigné: " + technicien.getNomUtilisateur() : ""));
    }

    public void addCommentaire(String comment, Utilisateur auteur) {
        Commentaire commentaire = new Commentaire(comment, auteur);
        listeCommentaires.add(commentaire);
    }

    @Override
    public String toString() {
        String comments = "";
        if (getComments() != null) {
            for (int j = 0; j < getComments().size(); j++) {
                comments += getComments().get(j).getCommentaire();
                comments += "&#" + getComments().get(j).getUtilisateur().getNomUtilisateur();
                if (getComments().size() - 1 == j) {
                    comments += "";
                } else {
                    comments += "/%";
                }
            }
        }
        final String sep = ";~";
        String path = "";
        String tech = "";
        String cat = getCategorie().getValueString();
        if (getTech() != null) {
            tech = getTech().getNomUtilisateur();
        }
        if (getFichier() != null) {
            path = getFichier().getPath();
        }
        String sSatisfaction = "";
        if (getSatisfaction() != null) {
            sSatisfaction = getSatisfaction().toString();
        }
        return (getSujet() + sep
                + getDescription() + sep
                + getClient().getNomUtilisateur() + sep
                + cat + sep
                + getStatut().getValueString() + sep
                + sSatisfaction + sep
                + path + sep + tech + sep
                + comments);
    }
}