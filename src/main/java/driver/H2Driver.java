package driver;

import org.h2.tools.RunScript;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class H2Driver {
    private static final String DB_URL = "jdbc:h2:mem:cranberrydb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;";
    private static final String DB_TEST_URL = "jdbc:h2:mem:cranberrydb_test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;";
    private static final String DB_BACKUP_FILE = "dat/cranberrydb";
    private static final String DB_BACKUP_TEST_FILE = "dat/cranberrydb_test";
    private static H2Driver instance = null;
    private static boolean isModeTest = false;
    private static String dbUrl;
    private static String dbBackupFile;
    private Connection connection;
    private Statement statement;

    private H2Driver() throws Exception {
        Class.forName("org.h2.Driver");
        if (dbUrl == null)
            dbUrl = DB_URL;
        if (dbBackupFile == null)
            dbBackupFile = DB_BACKUP_FILE;
        initConnection();
    }

    public static H2Driver getInstance() throws Exception {
        if (instance == null) {
            instance = new H2Driver();
        }
        return instance;
    }

    public static void setIsModeTest(boolean pIsModeTest) throws Exception {
        isModeTest = pIsModeTest;
        dbBackupFile = isModeTest ? null : DB_BACKUP_FILE;
        dbUrl = isModeTest ? DB_TEST_URL : DB_URL;
        instance = null;
    }

    private void initConnection() throws SQLException {
        connection = DriverManager.getConnection(dbUrl);
        if (!isModeTest) {
            try {
                RunScript.execute(connection, new FileReader(dbBackupFile));
            } catch (Exception ex) {
            }
        }
    }

    public Connection getConnection() throws SQLException {
        return connection;
    }

    public Statement getStatement() throws SQLException {
        if (statement != null && !statement.isClosed())
            statement.close();
        statement = connection.createStatement();
        return statement;
    }

    public void closeResources() throws SQLException {
        if (connection != null)
            connection.close();
        if (statement != null)
            statement.close();
    }

    public void persistData() throws SQLException {
        if (!isModeTest)
            getStatement().executeQuery(String.format("SCRIPT TO '%s'", DB_BACKUP_FILE));
    }

}
