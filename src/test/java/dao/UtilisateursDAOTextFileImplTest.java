package dao;

import models.Technicien;
import models.Utilisateur;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UtilisateursDAOTextFileImplTest {

    private UtilisateursDAOTextFileImpl utilisateursDAO;
    private Scanner scanner;

    private Technicien tech;
    private Utilisateur clientFromRepo;
    private List<Utilisateur> listeUtilisateur;

    @Before
    public void setUp() {
        clientFromRepo = new Utilisateur("Chabotte", "R", "chabotr", "monchatkiki");
        tech = new Technicien("Tech", "Guest", "techguest", "password");
        listeUtilisateur = new ArrayList<>();

        scanner = Mockito.mock(Scanner.class);
        utilisateursDAO = UtilisateursDAOTextFileImpl.getInstance(scanner);
    }

    @Test
    public void testGetAllLoadsContentFromRepoProperly() {
        Mockito.when(scanner.hasNextLine()).thenReturn(true).thenReturn(false);
        Mockito.when(scanner.nextLine()).thenReturn(clientFromRepo.toString());

        List<Utilisateur> allUsers = UtilisateursDAOTextFileImpl.getInstance(scanner).getAll();

        assertEquals(1, allUsers.size());
        assertEquals(clientFromRepo.toString(), allUsers.get(0).toString());

    }

    @Test
    public void testGetAllReturnsEmptyListWhenCantReadFile() {
        Mockito.when(scanner.hasNextLine()).thenReturn(true).thenReturn(false);
        Mockito.when(scanner.nextLine()).thenThrow(IOException.class);

        assertTrue(utilisateursDAO.getAll().isEmpty());
    }

}