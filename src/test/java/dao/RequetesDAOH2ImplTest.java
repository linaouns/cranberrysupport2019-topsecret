package dao;

import driver.H2Driver;
import models.Categorie;
import models.Requete;
import models.Utilisateur;
import org.junit.*;

import java.util.Arrays;

public class RequetesDAOH2ImplTest {

    private Requete requete01;
    private Requete requete02;
    private Utilisateur client;

    @AfterClass
    public static void afterClass() throws Exception {
        H2Driver.setIsModeTest(false);
    }

    @Before
    public void before() throws Exception {
        H2Driver.setIsModeTest(true);
        H2Driver.getInstance().getConnection().setAutoCommit(false);
    }

    @After
    public void after() throws Exception {
        H2Driver.getInstance().getConnection().rollback();
    }

    @Before
    public void setUp() {
        client = new Utilisateur("Po", "Poline", "popoline", "monchatkiki");
        requete01 = new Requete("Exemple 99", "Test d'une requete", client, Categorie.posteDeTravail);
        requete02 = new Requete("Exemple 100", "Test d'une requete", client, Categorie.posteDeTravail);
    }

    @Test
    public void testGetAllAfterSaving1Requete() throws Exception {
        Assert.assertEquals(1,
                RequetesDAOH2Impl.getInstance().save(Arrays.asList(requete01)));
        Assert.assertEquals(1, RequetesDAOH2Impl.getInstance().getAll().size());
        Assert.assertEquals(requete01.getSujet(), RequetesDAOH2Impl.getInstance().getAll().get(0).getSujet());
    }

    @Test
    public void testGetAllAfterSaving2Requetes() throws Exception {
        Assert.assertEquals(2,
                RequetesDAOH2Impl.getInstance().save(Arrays.asList(requete01, requete02)));
    }
}
