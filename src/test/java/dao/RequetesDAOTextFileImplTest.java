package dao;

import models.Categorie;
import models.Requete;
import models.Utilisateur;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import services.BanqueUtilisateurs;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RequetesDAOTextFileImplTest {
    private RequetesDAOTextFileImpl requetesDAO;
    private Scanner scanner;
    private BufferedWriter bufferedWriter;
    private BanqueUtilisateurs banqueUtilisateurs;

    private Requete requeteFromRepo;
    private Requete newRequete;
    private Utilisateur client;

    @Before
    public void setUp() {
        client = new Utilisateur("Po", "Poline", "popoline", "monchatkiki");
        requeteFromRepo = new Requete("Exemple 99", "Test d'une requete", client, Categorie.posteDeTravail);
        newRequete = new Requete("Exemple 100", "Test d'une requete", client, Categorie.posteDeTravail);

        scanner = Mockito.mock(Scanner.class);
        bufferedWriter = Mockito.mock(BufferedWriter.class);
        banqueUtilisateurs = Mockito.mock(BanqueUtilisateurs.class);

        requetesDAO = RequetesDAOTextFileImpl.getInstance(scanner, bufferedWriter, banqueUtilisateurs);
    }

    @Test
    public void testGetAllLoadsContentFromRepoProperly() {
        Mockito.when(scanner.hasNextLine()).thenReturn(true).thenReturn(false);
        Mockito.when(scanner.nextLine()).thenReturn(requeteFromRepo.toString());
        Mockito.when(banqueUtilisateurs.chercheUtilisateurParUserName(client.getNomUtilisateur())).thenReturn(client);

        List<Requete> allRequetes = RequetesDAOTextFileImpl.getInstance(scanner, bufferedWriter, banqueUtilisateurs).getAll();

        assertEquals(requeteFromRepo.toString(), allRequetes.get(0).toString());
        assertEquals(1, allRequetes.size());
    }

    @Test
    public void testGetAllReturnsEmptyListWhenCantReadFile() {
        Mockito.when(scanner.hasNextLine()).thenReturn(true).thenReturn(false);
        Mockito.when(scanner.nextLine()).thenThrow(IOException.class);

        assertTrue(requetesDAO.getAll().isEmpty());
    }

    @Test
    public void testSaveRequetesReturnsRightNumberOfRequestsSaved() throws IOException {
        Mockito.doNothing().when(bufferedWriter).write(Mockito.anyString());
        List<Requete> requetesToSave = new ArrayList<>();

        requetesToSave.add(requeteFromRepo);
        assertEquals(1, requetesDAO.save(requetesToSave));

        requetesToSave.add(newRequete);
        assertEquals(2, requetesDAO.save(requetesToSave));
    }

    @Test
    public void testSaveRequetesReturns0WhenCantWriteInFile() throws IOException {
        Mockito.doThrow(IOException.class).when(bufferedWriter).write(Mockito.anyString());
        int nbRequetesSaved = 0;

        assertEquals(nbRequetesSaved, requetesDAO.save(Arrays.asList(requeteFromRepo)));
    }

}
