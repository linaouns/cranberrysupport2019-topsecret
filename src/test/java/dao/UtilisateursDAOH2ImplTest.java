package dao;

import driver.H2Driver;
import models.Technicien;
import models.Utilisateur;
import org.junit.*;

import java.util.Arrays;

public class UtilisateursDAOH2ImplTest {

    private Utilisateur user01;
    private Utilisateur user02;

    @AfterClass
    public static void afterClass() throws Exception {
        H2Driver.setIsModeTest(false);
    }

    @Before
    public void before() throws Exception {
        H2Driver.setIsModeTest(true);
        H2Driver.getInstance().getConnection().setAutoCommit(false);
    }

    @After
    public void after() throws Exception {
        H2Driver.getInstance().getConnection().rollback();
    }

    @Before
    public void setUp() {
        user01 = new Utilisateur("Po", "Poline", "popoline", "monchatkiki");
        user02 = new Technicien("Tech", "Guest", "techguest", "password");
    }

    @Test
    public void testGetAllAfterSaving1User() throws Exception {
        Assert.assertEquals(1,
                UtilisateursDAOH2Impl.getInstance().save(Arrays.asList(user01)));
        Assert.assertEquals(1, UtilisateursDAOH2Impl.getInstance().getAll().size());
        Assert.assertEquals(user01.getNomUtilisateur(), UtilisateursDAOH2Impl.getInstance().getAll().get(0).getNomUtilisateur());
    }

    @Test
    public void testGetAllAfterSaving2Users() throws Exception {
        Assert.assertEquals(2,
                UtilisateursDAOH2Impl.getInstance().save(Arrays.asList(user01, user02)));
        Assert.assertEquals(2, UtilisateursDAOH2Impl.getInstance().getAll().size());
    }

}
