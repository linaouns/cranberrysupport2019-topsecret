package models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UtilisateurTest {
    Utilisateur client = new Utilisateur("Nom", "Test", "nomTest", "motedepasstest");
    Requete req1 = new Requete("Exemple 88", "Test d'une requete", client, Categorie.posteDeTravail);

    @Test
    public void testGetRole() {
        assertEquals(Utilisateur.Role.client.toString(), client.getRole());
    }

    @Test
    public void testAjoutRequeteUpdatesListeRequetes() {
        assertTrue(client.getRequetesCreees().isEmpty());
        client.ajouterRequeteCreee(req1);
        assertEquals(1, client.getRequetesCreees().size());
    }
}