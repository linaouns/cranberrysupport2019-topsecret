package models;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RequeteTest {
    private Utilisateur client = new Utilisateur("Chabotte", "R", "chabotr", "monchatkiki");
    private Requete req = new Requete("Exemple 88", "Test d'une requete", client, Categorie.posteDeTravail);
    private String nouveauComment = "commentaire test";

    public RequeteTest() throws IOException {
    }

    @Test
    public void addCommentaire() throws IOException {
        assertTrue(req.getComments().isEmpty());

        req.addCommentaire(nouveauComment, client);

        assertEquals(1, req.getComments().size());
        assertEquals(nouveauComment, req.getComments().get(0).getCommentaire());
    }

    @Test
    public void setSujet() throws IOException {
        req.setSujet("sujet test");
        assertEquals("sujet test", req.getSujet());
    }

    @Test
    public void setDescription() throws IOException {
        req.setDescription("description test");
        assertEquals("description test", req.getDescription());
    }

    @Test
    public void setCategorie() throws IOException {
        req.setCategorie(Categorie.posteDeTravail);
        assertEquals(Categorie.posteDeTravail, req.getCategorie());
    }

    @Test
    public void setSatisfaction() throws IOException {
        req.setSatisfaction(true);
        assertEquals(true, req.getSatisfaction());
    }

}
