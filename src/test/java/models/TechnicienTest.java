package models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TechnicienTest {
    private Technicien tech = new Technicien("prenomTest", "nomTest", "usenrame", "motedepasstest");
    private Utilisateur client = new Utilisateur("nom", "Test", "nomTest", "motedepasstest");
    private Requete req1 = new Requete("Exemple 88", "Test d'une requete", client, Categorie.posteDeTravail);

    @Test
    public void ajouterRequeteAssignee() {
        tech.assignerRequete(req1);
        assertEquals(1, tech.getRequetesAssignees().size());
    }

    @Test
    public void ajoutRequete() {
        tech.assignerRequete(req1);
        assertEquals(1, tech.getRequetesAssignees().size());
    }
}