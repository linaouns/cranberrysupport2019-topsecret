package services;

import dao.UtilisateursDAO;
import models.Categorie;
import models.Requete;
import models.Technicien;
import models.Utilisateur;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class BanqueUtilisateursTest {
    private UtilisateursDAO utilisateursDAO;
    private BanqueUtilisateurs banqueUtilisateurs;

    private Technicien tech;
    private Utilisateur clientFromRepo;
    private Requete requete;
    private List<Utilisateur> utilisateursFromRepo = new ArrayList<>();
    private List<Utilisateur> listeUtilisateur = new ArrayList<>();

    @Before
    public void setUp() throws FileNotFoundException {
        tech = new Technicien("Tech", "Guest", "techguest", "password");
        clientFromRepo = new Utilisateur("Chabotte", "R", "chabotr", "monchatkiki");
        requete = new Requete("req", "descr", clientFromRepo, Categorie.posteDeTravail);
        utilisateursFromRepo.add(clientFromRepo);
        listeUtilisateur = new ArrayList<>();

        utilisateursDAO = Mockito.mock(UtilisateursDAO.class);
        Mockito.when(utilisateursDAO.getAll()).thenReturn(utilisateursFromRepo);

        banqueUtilisateurs = BanqueUtilisateurs.getUserInstance(utilisateursDAO);
    }

    @Test
    public void testChargerUtilisateurUpdatesListeRequetes() {
        assertEquals(1, banqueUtilisateurs.getUtilisateurs().size());
        assertEquals(clientFromRepo.toString(), banqueUtilisateurs.getUtilisateurs().get(0).toString());
    }

    @Test
    public void testAddUtilisateurUpdatesListeUtilisateurs() {
        assertEquals(1, banqueUtilisateurs.getUtilisateurs().size());

        banqueUtilisateurs.registerAccount(tech.getPrenom(), tech.getNom(),
                tech.getNomUtilisateur(), tech.getMotDePasse(), tech.getRole());

        assertEquals(2, banqueUtilisateurs.getUtilisateurs().size());
        assertEquals(tech.toString(), banqueUtilisateurs.getUtilisateurs().get(1).toString());
    }

    @Test
    public void testChercherNomRole() {
        listeUtilisateur = banqueUtilisateurs.getUtilisateursByRole(Utilisateur.Role.client.toString());
        for (Utilisateur utilisateur : listeUtilisateur) {
            assertEquals(Utilisateur.Role.client.toString(), utilisateur.getRole());
        }

        listeUtilisateur = banqueUtilisateurs.getUtilisateursByRole(Utilisateur.Role.technicien.toString());
        for (Utilisateur utilisateur : listeUtilisateur) {
            assertEquals(Utilisateur.Role.technicien.toString(), utilisateur.getRole());
        }
    }

    @Test
    public void testChercherUtilisateurByUsername() {
        assertEquals(clientFromRepo.toString(),
                banqueUtilisateurs.chercheUtilisateurParUserName(clientFromRepo.getNomUtilisateur()).toString());
    }

    @Test
    public void testLockAccountClient() {
        clientFromRepo.closeAccount();
        assertEquals(true, clientFromRepo.isLocked());
    }

    @Test
    public void testLockAccountTech() {
        requete.setTech(tech);
        assertEquals(1, tech.getRequetesAssignees().size());
        tech.closeAccount();
        assertEquals(true, tech.isLocked());
        assertEquals(0, tech.getRequetesAssignees().size());
    }
}