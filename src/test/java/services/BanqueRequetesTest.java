package services;

import dao.RequetesDAO;
import models.Categorie;
import models.Requete;
import models.Statut;
import models.Technicien;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class BanqueRequetesTest {
    private RequetesDAO requetesDAO;
    private BanqueRequetes banqueRequetes;

    private List<Requete> requetesFromRepo = new ArrayList<>();
    private Requete requeteFromRepo;
    private Requete newRequete;
    private Technicien tech;

    @Before
    public void setUp() {
        tech = new Technicien("Tech", "Guest", "techguest", "password");
        requeteFromRepo = new Requete("Exemple 99", "Test d'une requete", tech, Categorie.posteDeTravail);
        requetesFromRepo.add(requeteFromRepo);
        newRequete = new Requete("Exemple 100", "Test d'une requete", tech, Categorie.posteDeTravail);

        requetesDAO = Mockito.mock(RequetesDAO.class);
        Mockito.when(requetesDAO.getAll()).thenReturn(requetesFromRepo);
        banqueRequetes = BanqueRequetes.getInstance(requetesDAO);
    }

    @Test
    public void testChargerRequetesUpdatesListeRequetes() {
        assertEquals(1, banqueRequetes.getListeRequetes().size());
        assertEquals(requeteFromRepo.toString(), banqueRequetes.getListeRequetes().get(0).toString());
    }

    @Test
    public void testGetRequetesByStatut() {
        banqueRequetes.newRequete(newRequete.getSujet(), newRequete.getDescription(), newRequete.getClient(), newRequete.getCategorie());
        newRequete.setStatut(Statut.enTraitement);
        banqueRequetes.getListeRequetes().get(1).setStatut(Statut.enTraitement);

        assertEquals(1, banqueRequetes.getListRequetesByStatut(Statut.ouvert).size());
        assertEquals(requeteFromRepo, banqueRequetes.getListRequetesByStatut(Statut.ouvert).get(0));

        assertEquals(1, banqueRequetes.getListRequetesByStatut(Statut.enTraitement).size());
        assertEquals(newRequete.toString(), banqueRequetes.getListRequetesByStatut(Statut.enTraitement).get(0).toString());

        assertEquals(0, banqueRequetes.getListRequetesByStatut(Statut.finalAbandon).size());
        assertEquals(0, banqueRequetes.getListRequetesByStatut(Statut.finalSucces).size());
    }

    @Test
    public void testReturnLast() {
        assertEquals(requeteFromRepo.toString(), banqueRequetes.returnLast().toString());
    }

    @Test
    public void testAddNewRequeteUpdatesListeRequetes() {
        assertEquals(1, banqueRequetes.getListeRequetes().size());

        banqueRequetes.newRequete(newRequete.getSujet(), newRequete.getDescription(), newRequete.getClient(), newRequete.getCategorie());

        assertEquals(2, banqueRequetes.getListeRequetes().size());
        assertEquals(newRequete.toString(), banqueRequetes.getListeRequetes().get(1).toString());
    }

    @Test
    public void testSaveRequetesBeforeAddNewRequete() {
        Mockito.when(requetesDAO.save(Mockito.anyList())).thenReturn(banqueRequetes.getListeRequetes().size());

        assertEquals(1, banqueRequetes.saveRequetes());
    }

    @Test
    public void saveRequetesAfterAddNewRequete() {
        banqueRequetes.newRequete(newRequete.getSujet(), newRequete.getDescription(),
                newRequete.getClient(), newRequete.getCategorie());

        Mockito.when(requetesDAO.save(Mockito.anyList())).thenReturn(banqueRequetes.getListeRequetes().size());

        assertEquals(2, banqueRequetes.saveRequetes());
    }

}